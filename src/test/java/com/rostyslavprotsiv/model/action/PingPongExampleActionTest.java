package com.rostyslavprotsiv.model.action;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class PingPongExampleActionTest {

    @Test
    public void testShow() {
        PingPongExampleAction action = new PingPongExampleAction();
        String possiblePingPongResult1 = "ping pong ping pong ping pong ping " +
                "pong ping pong ping pong ping pong ping pong ping pong ping " +
                "pong ";
        String possiblePingPongResult2 = "pong ping pong ping pong ping pong " +
                "ping pong ping pong ping pong ping pong ping pong ping pong " +
                "ping ";
        String actionResult = action.show();
        assertTrue(actionResult.equals(possiblePingPongResult1)
                || actionResult.equals(possiblePingPongResult2));
    }
}
