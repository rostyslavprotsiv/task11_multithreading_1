package com.rostyslavprotsiv.model.action;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class FibonacciActionTest {

    @Test
    public void testGetFibonacciSequence() {
        FibonacciAction action = new FibonacciAction();
        List<Integer> fibonacciRange = Arrays.asList(0, 1, 1, 2);
        assertTrue(action.getFibonacciSequence(4)
                .equals(fibonacciRange));
    }
}
