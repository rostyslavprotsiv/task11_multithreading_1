package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public final class Controller {
    private final Logger LOGGER = LogManager.getLogger(Controller.class);
    private final PingPongExampleAction PING_PONG_EXAMPLE_ACTION =
            new PingPongExampleAction();
    private final FibonacciAction FIBONACCI_ACTION = new FibonacciAction();
    private final SleepAction SLEEP_ACTION = new SleepAction();
    private final SynchronizationExampleAction SYNCHRONIZATION_EXAMPLE_ACTION =
            new SynchronizationExampleAction();
    private final PipeCommunicationAction PIPE_COMMUNICATION_ACTION =
            new PipeCommunicationAction();

    public String showPingPongExample() {
        return PING_PONG_EXAMPLE_ACTION.show();
    }

    public List<Integer> getFibonacciSeq(int n) {
        return FIBONACCI_ACTION.getFibonacciSequence(n);
    }

    public List<List<Integer>> getListOfFibonacciNumbers(Integer... amount) {
        return FIBONACCI_ACTION.getListOfFibonacciNumbers(amount);
    }

    public List<Integer> getFibonacciSeqWithScheduled(int n) {
        return FIBONACCI_ACTION.getFibonacciWithScheduled(n);
    }

    public List<List<Integer>> getListOfFibWithPool(Integer... amount) {
        return FIBONACCI_ACTION.getListOfFibonacciNumbersWithPool(amount);
    }

    public int getFibSum(int n) {
        return FIBONACCI_ACTION.getFibonacciSum(n);
    }

    public List<Integer> getSumOfFibNumbers(Integer... amount) {
        return FIBONACCI_ACTION.getSumsOfFibonacciNumbers(amount);
    }

    public void sleepTasks(int quantity, List<Integer> time) {
        SLEEP_ACTION.doSleepTasks(quantity, time);
    }

    public void getExampleWithOneObj(StringBuilder b) {
        SYNCHRONIZATION_EXAMPLE_ACTION.showExampleWithOneObj(b);
    }

    public void getExampleWithDifObj(StringBuilder b) {
        SYNCHRONIZATION_EXAMPLE_ACTION.showExampleWithDifObj(b);
    }

    public String writeAndRead(String str) {
        String result = PIPE_COMMUNICATION_ACTION.writeAndRead(str);
        if (result == null) {
            String logMessage = "Error with pipes(== null)";
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
        }
        return result;
    }
}
