package com.rostyslavprotsiv.view;

import com.rostyslavprotsiv.view.interfaces.GetableExample;

import java.util.LinkedList;
import java.util.List;

public final class Menu extends AbstractMenu {

    public Menu() {
        menu.put(0, "Ping-Pong example");
        menu.put(1, "Fibonacci numbers(a lot of lists): ");
        menu.put(2, "Fibonacci numbers(one list): ");
        menu.put(3, "Fibonacci numbers(a lot of lists): ");
        menu.put(4, "Fibonacci numbers(one list) and delay before: ");
        menu.put(5, "Sum of Fibonacci numbers : ");
        menu.put(6, "Sums of a lot of amount of Fibonacci numbers : ");
        menu.put(7, "Show sleep tasks");
        menu.put(8, "Print example with the same object in synchronization " +
                "blocks");
        menu.put(9, "Print example with many different objects in " +
                "synchronization blocks");
        menu.put(10, "Show communication between two tasks with the help of " +
                "pipes");
        menu.put(11, "Exit");
        methodsForMenu.put(0, this::printPingPongExample);
        methodsForMenu.put(1, this::printAllFibonacciNumbers);
        methodsForMenu.put(2, this::printGotFibonacciSeq);
        methodsForMenu.put(3, this::printAllFibonacciNumbersWithPoll);
        methodsForMenu.put(4, this::printGotFibonacciSeqWithScheduled);
        methodsForMenu.put(5, this::printFibSum);
        methodsForMenu.put(6, this::printSumOfFibNumbers);
        methodsForMenu.put(7, this::printSleepTasks);
        methodsForMenu.put(8, this::printExampleWithOneObj);
        methodsForMenu.put(9, this::printExampleWithDifObj);
        methodsForMenu.put(10, this::showWriteAndRead);
        methodsForMenu.put(11, this::quit);
    }

    public void show() {
        out();
    }

    @Override
    protected void showInfo() {
        LOGGER.info("Menu for task11_MultiThreading_1");
        menu.forEach((key, elem) -> LOGGER.info(key + " : " + elem));
    }

    private void printPingPongExample() {
        LOGGER.info(CONTROLLER.showPingPongExample());
    }

    private List<Integer> getInputOfAmount() {
        List<Integer> inputted = new LinkedList<>();
        int inp;
        LOGGER.info("Please, input amount of amount of numbers Fibonacci");
        while (scan.hasNextInt() && (inp = scan.nextInt()) > 0) {
            inputted.add(inp);
        }
        return inputted;
    }

    private void printAllFibonacciNumbers() {
        List<Integer> inputted = getInputOfAmount();
        CONTROLLER.getListOfFibonacciNumbers(inputted.toArray(new Integer[inputted.size()]))
                .forEach(LOGGER::info);
    }

    private void printGotFibonacciSeq() {
        int inp;
        LOGGER.info("Please, input the number of Fibonacci range : ");
        if (scan.hasNextInt() && (inp = scan.nextInt()) > 0) {
            LOGGER.info(CONTROLLER.getFibonacciSeq(inp));
        } else {
            badInput();
        }
    }

    private void printAllFibonacciNumbersWithPoll() {
        List<Integer> inputted = getInputOfAmount();
        CONTROLLER.getListOfFibWithPool(inputted.toArray(new Integer[inputted.size()]))
                .forEach(LOGGER::info);
    }

    private void printGotFibonacciSeqWithScheduled() {
        int inp;
        LOGGER.info("Please, input the number of Fibonacci range : ");
        if (scan.hasNextInt() && (inp = scan.nextInt()) > 0) {
            LOGGER.info(CONTROLLER.getFibonacciSeqWithScheduled(inp));
        } else {
            badInput();
        }
    }

    private void printSumOfFibNumbers() {
        List<Integer> inputted = getInputOfAmount();
        LOGGER.info(CONTROLLER.getSumOfFibNumbers(
                inputted.toArray(new Integer[inputted.size()])));
    }

    private void printFibSum() {
        int inp;
        LOGGER.info("Please, input the amount of Fibonacci numbers : ");
        if (scan.hasNextInt() && (inp = scan.nextInt()) > 0) {
            LOGGER.info(CONTROLLER.getFibSum(inp));
        } else {
            badInput();
        }
    }

    private void printSleepTasks() {
        LOGGER.info("Please, input the amount of tasks : ");
        List<Integer> time = new LinkedList<>();
        int inp;
        if (scan.hasNextInt() && (inp = scan.nextInt()) > 0) {
            new Thread(() -> CONTROLLER.sleepTasks(inp, time)).start();
            Thread listener = new Thread(() -> timeListener(time, inp));
            listener.start();
            try {
                listener.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            String logMessage = "Used incorrect amount";
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
            LOGGER.fatal(logMessage);
            badInput();
        }
    }

    private void timeListener(List<Integer> time, int maxSize) {
        int size = 0;
        while (time.size() != maxSize) {
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (time.size() > size) {
                LOGGER.info(((LinkedList<Integer>) time).getLast());
                size = time.size();
            }
        }
    }

    private void printExampleWithOneObj() {
        printExampleWith(CONTROLLER::getExampleWithOneObj);
    }

    private void printExampleWith(GetableExample get) {
        StringBuilder b = new StringBuilder();
        Thread listener = new Thread(() -> bListener(b));
        listener.start();
        get.getExample(b);
        try {
            listener.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void printExampleWithDifObj() {
        printExampleWith(CONTROLLER::getExampleWithDifObj);
    }

    private void bListener(StringBuilder b) {
        int maxCalls = 3;
        StringBuilder bOld = new StringBuilder(b);
        int counter = 0;
        while (counter != maxCalls) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (!b.toString()
                    .equals(bOld.toString())) {
                LOGGER.info(b.toString());
                bOld = new StringBuilder(b);
                counter++;
            }
        }
    }

    private void showWriteAndRead() {
        LOGGER.info("Please, input one string");
        LOGGER.info(CONTROLLER.writeAndRead(scan.next() + '\n'));
    }
}
