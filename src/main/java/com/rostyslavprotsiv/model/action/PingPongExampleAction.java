package com.rostyslavprotsiv.model.action;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PingPongExampleAction {
    private volatile static StringBuilder pingPongResult = new StringBuilder();
    private static Object sync = new Object();
    private final Logger LOGGER =
            LogManager.getLogger(PingPongExampleAction.class);

    private void doPing() {
        synchronized (sync) {
            for (int i = 0; i < 10; i++) {
                sync.notify();
                try {
                    sync.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                pingPongResult.append("ping ");
                sync.notify();
            }
        }
    }

    private void doPong() {
        synchronized (sync) {
            for (int i = 0; i < 10; i++) {
                sync.notify();
                try {
                    sync.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                pingPongResult.append("pong ");
                sync.notify();
            }
        }
    }

    public String show() {
        pingPongResult = new StringBuilder();
        Thread t1 = new Thread(this::doPing);
        Thread t2 = new Thread(this::doPong);
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            String logMessage = e.getMessage();
            LOGGER.trace(logMessage);
            LOGGER.debug(logMessage);
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
            LOGGER.fatal(logMessage);
            e.printStackTrace();
        }
        return pingPongResult.toString();
    }
}