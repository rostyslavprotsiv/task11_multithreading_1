package com.rostyslavprotsiv.model.action;

import java.io.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PipeCommunicationAction {
    private PipedReader pipedReader;
    private PipedWriter pipedWriter;


    private Runnable runnable(String str) {
        return () -> {
            try {
                pipedWriter.write(str);
            } catch (IOException e) {
                e.printStackTrace();
            }
        };
    }

    private Callable<String> callable() {
        return () -> new BufferedReader(pipedReader).readLine();
    }

    public String writeAndRead(String str) {
        pipedReader = new PipedReader();
        pipedWriter = new PipedWriter();
        int threadAmount = 2;
        ExecutorService executorService =
                Executors.newFixedThreadPool(threadAmount);
        String read = null;
        try {
            pipedReader.connect(pipedWriter);
            executorService.submit(runnable(str)).get();
            read = executorService.submit(callable()).get();
            pipedReader.close();
            pipedWriter.close();
        } catch (InterruptedException | ExecutionException | IOException e) {
            e.printStackTrace();
        }
        executorService.shutdown();
        return read;
    }
}
