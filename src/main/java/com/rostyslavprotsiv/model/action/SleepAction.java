package com.rostyslavprotsiv.model.action;

import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class SleepAction {

    private static synchronized Callable<Integer> callable() {
        return () -> {
            int minTime = 1;
            int maxTime = 10;
            int secondsMultiplier = 1000;
            Random random = new Random();
            int timeForSleep = random.nextInt(maxTime) + minTime;
            try {
                Thread.sleep(timeForSleep * secondsMultiplier);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return timeForSleep;
        };
    }

    public void doSleepTasks(int quantity, List<Integer> time) {
        ScheduledExecutorService ex = Executors.newScheduledThreadPool(quantity);
        for (int i = 0; i < quantity; i++) {
            try {
                time.add(ex.submit(callable()).get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        ex.shutdown();
    }
}
