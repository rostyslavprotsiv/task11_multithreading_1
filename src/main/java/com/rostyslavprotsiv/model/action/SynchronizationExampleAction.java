package com.rostyslavprotsiv.model.action;

public class SynchronizationExampleAction {
    private static Object obj = new Object();
    private static Object obj1 = new Object();
    private static Object obj2 = new Object();
    private static Object obj3 = new Object();

    private void doSmth(StringBuilder b) {
        //some logic
        synchronized (obj) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (b.length() == 0) {
                b.append(2);
            } else {
                b.deleteCharAt(0);
                b.append(2);
            }
        }
    }

    private void doSmth1(StringBuilder b) {
        //some logic
        synchronized (obj) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (b.length() == 0) {
                b.append(3);
            } else {
                b.deleteCharAt(0);
                b.append(3);
            }
        }
    }

    private void doSmth2(StringBuilder b) {
        //some logic
        synchronized (obj) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (b.length() == 0) {
                b.append(4);
            } else {
                b.deleteCharAt(0);
                b.append(4);
            }
        }
    }

    private void doSmthWithOther(StringBuilder b) {
        //some logic
        synchronized (obj1) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (b.length() == 0) {
                b.append(2);
            } else {
                b.deleteCharAt(0);
                b.append(2);
            }
        }
    }

    private void doSmthWithOther1(StringBuilder b) {
        //some logic
        synchronized (obj2) {
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (b.length() == 0) {
                b.append(3);
            } else {
                b.deleteCharAt(0);
                b.append(3);
            }
        }
    }

    private void doSmthWithOther2(StringBuilder b) {
        //some logic
        synchronized (obj3) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (b.length() == 0) {
                b.append(4);
            } else {
                b.deleteCharAt(0);
                b.append(4);
            }
        }
    }

    public void showExampleWithOneObj(StringBuilder b) {
        new Thread(() -> doSmth(b)).start();
        new Thread(() -> doSmth1(b)).start();
        new Thread(() -> doSmth2(b)).start();
    }

    public void showExampleWithDifObj(StringBuilder b) {
        new Thread(() -> doSmthWithOther(b)).start();
        new Thread(() -> doSmthWithOther1(b)).start();
        new Thread(() -> doSmthWithOther2(b)).start();
    }
}
