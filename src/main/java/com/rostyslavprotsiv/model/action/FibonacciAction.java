package com.rostyslavprotsiv.model.action;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;

public class FibonacciAction {
    private static List<Integer> fibonacciSeq = new LinkedList<>();

    private static Callable<Integer> callable(int n) {
        return () -> {
            if (n <= 0) {
                throw new IllegalArgumentException("n must be > 0");
            }
            int previousNumber = 0;
            int currentNumber = 1;
            int temporary;
            int sum = 0;
            if (n > 1) {
                sum = 1;
            }
            for (int i = 0; i < n - 2; i++) {
                temporary = currentNumber;
                currentNumber += previousNumber;
                previousNumber = temporary;
                sum += currentNumber;
            }
            return sum;
        };
    }

    private static synchronized Runnable runnable(int n) {
        return () -> {
            if (n <= 0) {
                throw new IllegalArgumentException("n must be > 0");
            }
            int previousNumber = 0;
            int currentNumber = 1;
            int temporary;
            if (n == 1) {
                fibonacciSeq.add(0);
            } else {
                fibonacciSeq.add(0);
                fibonacciSeq.add(1);
                for (int i = 0; i < n - 2; i++) {
                    temporary = currentNumber;
                    currentNumber += previousNumber;
                    previousNumber = temporary;
                    fibonacciSeq.add(currentNumber);
                }
            }
        };
    }

    public List<Integer> getFibonacciSequence(int n) {
        fibonacciSeq = new LinkedList<>();
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Runnable task = runnable(n);
        try {
            executor.submit(task)
                    .get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        executor.shutdown();
        return fibonacciSeq;
    }

    public List<List<Integer>> getListOfFibonacciNumbers(Integer... numbers) {
        fibonacciSeq = new LinkedList<>();
        List<List<Integer>> allResult = new LinkedList<>();
        int defaultThreadPool = 10;
        ExecutorService executor =
                Executors.newFixedThreadPool(defaultThreadPool);
        Arrays.stream(numbers)
                .forEach(n -> {
                    fibonacciSeq = new LinkedList<>();
                    Future future = executor.submit(runnable(n));
                    try {
                        future.get();
                        allResult.add(fibonacciSeq);
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                    }
                });
        executor.shutdown();
        return allResult;
    }

    public List<Integer> getFibonacciWithScheduled(int n) {
        ScheduledExecutorService executor =
                Executors.newSingleThreadScheduledExecutor();
        fibonacciSeq = new LinkedList<>();
        Runnable task = runnable(n);
        ScheduledFuture<?> future = executor.schedule(task, 3,
                TimeUnit.SECONDS);
        fibonacciSeq.add((int) future.getDelay(TimeUnit.MILLISECONDS));
        try {
            future.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        executor.shutdown();
        return fibonacciSeq;
    }

    public List<List<Integer>> getListOfFibonacciNumbersWithPool(Integer...
                                                                         numbers) {
        List<List<Integer>> allResult = new LinkedList<>();
        int corePoolSize = 5;
        int maxPoolSize = 10;
        long keepAliveTime = 5000;
        int queueSpace = 512;
        fibonacciSeq = new LinkedList<>();
        ExecutorService threadPoolExecutor =
                new ThreadPoolExecutor(corePoolSize, maxPoolSize,
                        keepAliveTime,
                        TimeUnit.MILLISECONDS,
                        new LinkedBlockingQueue<>(queueSpace));
        Arrays.stream(numbers)
                .forEach(n -> {
                    fibonacciSeq = new LinkedList<>();
                    try {
                        threadPoolExecutor.submit(runnable(n))
                                .get();
                        allResult.add(fibonacciSeq);
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                    }
                });
        threadPoolExecutor.shutdown();
        return allResult;
    }

    public int getFibonacciSum(int n) {
        ExecutorService executorService =
                Executors.newSingleThreadExecutor();
        Callable<Integer> task = callable(n);
        Future<Integer> futureSeq = executorService.submit(task);
        executorService.shutdown();
        try {
            return futureSeq.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        //program ended with error
        return -1;
    }

    public List<Integer> getSumsOfFibonacciNumbers(Integer... numbers) {
        List<Integer> allSums = new LinkedList<>();
        int defaultThreadPool = 10;
        ExecutorService executorService =
                Executors.newFixedThreadPool(defaultThreadPool);
        List<Callable<Integer>> callables = new LinkedList<>();
        Arrays.stream(numbers)
                .forEach(n -> callables.add(callable(n)));
        try {
            List<Future<Integer>> future =
                    executorService.invokeAll(callables);
            executorService.shutdown();
            future.stream()
                    .map(f -> {
                        try {
                            return f.get();
                        } catch (InterruptedException | ExecutionException e) {
                            e.printStackTrace();
                        }
                        return null;
                    })
                    .forEach(allSums::add);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return allSums;
    }
}
